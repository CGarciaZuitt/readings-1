/* 
Create a simple server and the following routes with their corresponding HTTP methods and responses:

Assign the port to 4000
	
	If the url is "/", send a response "Hi, welcome back."

 	If the url is "/profile", send a response "Let's go over your profile"

	If the url is "/courses", send a response "Here’s our available courses"

	If the url is "/addCourse", send a response "This will be used for adding courses"

	If the url is "/updateCourse", send a response "This will be used for updating courses"
	
	If the url is "/archiveCourse", send a response "This will be used for archiving courses"


Test all the endpoints in Postman.
*/

let http = require("http");

http.createServer(function (request, response) {

	if(request.url == "/" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to booking system');

	}

	if(request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile');

	}

	// Retrieving a course
	if(request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Here's our courses available`);

	}

	// Adding a new course
	if(request.url = "/addCourse" && request.method == "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add course to our resources');

	}

	// Updating a course
	if(request.url = "/updateCourse" && request.method == "PUT"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources');

	}

	// Deleting a course
	if(request.url = "/archiveCourses" && request.method == "DELETE"){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive courses to our resources');

	}

}).listen(4000);

console.log('Server running at localhost:4000');
