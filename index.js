// console.log('hi')

// For Loops exercises

// 1. Create for loop that will iterate from 0 to 15. For each iteration, it will check if the current number is odd or even, and print out a message in the browser
	// Sample Output:
	/*
		0 is even
		1 is odd
		2 is even
		3 is odd
		4 is even
		5 is odd
		(until 15)
	*/

for (let x = 0; x <= 15; x++) {
	if (x === 0) {
	   console.log(x +  " is even");

	} else if (x % 2 === 0) {
	   console.log(x + " is even");   

	} else {
	   console.log(x + " is odd");
	}
};

/*2. Create a loop which iterates the integers from 1 to 100. Following the conditions below:
	a. numbers divisible by 3 print "Fizz" instead of the number
	b. numbers divisible by 5 print "Buzz". 
	c. For numbers which are divisible of both three and five print "FizzBuzz". 

	Sample Output:
		1  
		2
		Fizz 
		4 
		Buzz 
		Fizz 
		7 
		8 
		Fizz 
		Buzz 
		11 
		Fizz 
		13
		14 
		FizzBuzz
*/
	

for (let i = 1; i <= 100; i++){
  
  if ( i % 3 === 0 && i % 5 === 0 ){
    console.log("FizzBuzz");

  } else if ( i % 3 === 0 ){
    console.log("Fizz");

  } else if ( i % 5 === 0 ){
    console.log("Buzz");

  } else {
    console.log(i);
  }
};

// Array Methods

/*

	>> Create an addStudent() function that will accept a name of the student and add it to the student array.

	>> Create a countStudents() function that will print the total number of students in the array.

	>> Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

	// Stretch Goal:
	>> Create a findStudent() function that will do the following:
		--Search for a student name when a keyword is given (filter method).
			-- filter method returns a new array
			-- inside filter method function, use includes method to check if the keyword exists
		--If one match is found print the message studentName is an enrollee.
		--If multiple matches are found print the message studentNames are enrollees.
		--If no match is found print the message studentName is not an enrollee.
		--The keyword given should not be case sensitive.
*/

let students = [];

function addStudent(name) {

    students.push(name);

    console.log(`${name} was added to the student's list.`);

}

function countStudents() {

    console.log(`There are a total of ${students.length} students enrolled.`);

}

function printStudents() {

    // Sorts the array elements in alphanumeric order
    students.sort();

    // Loops through all elements of the array printing all the names of the students
    students.forEach(function(student) {
        // Prints the name of the students one by one
        console.log(student);
    })

}

function findStudent(keyword) {

    // Loops through all elements of the array to find the student name that matches the keyword provided
    let match = students.filter((student) => {

        // If the student name includes the keyword provided
        return student.toLowerCase().includes(keyword.toLowerCase());

    })

    console.log(match);

    // If a match was found the array's length will be 1
    if (match.length == 1) {

        console.log(`${match} is an Enrollee'`);

    // If multiple elements are found
    } else if (match.length > 1) {

        console.log(`${match} are enrollees`);

    // If no match was found the array's length will be 0
    } else {

        console.log(`No student found with the name ${keyword}`);

    }
}

// Node JS Server 

